package be.xplore.twitterwall.conferenceservice.controllers.rest;

import be.xplore.twitterwall.conferenceservice.domain.Talk;
import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;
import be.xplore.twitterwall.conferenceservice.web.controllers.rest.ConferenceRestController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConferenceRestController.class)
public class ConferenceRestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ConferenceRestController conferenceRestController;

    private List<TalkSlot> talkSlots;

    @Before
    public void setUp() {
        Talk talk = new Talk("Talk title", new ArrayList<>(), new ArrayList<>());
        TalkSlot talkSlot = new TalkSlot("09:30", "11:30", "Room 6", "monday", talk);
        talkSlots = Collections.singletonList(talkSlot);

    }

    @Test
    public void testGetTalksByDay() throws Exception {

        given(conferenceRestController.getTalkSlotsByDay("monday")).willReturn(new ResponseEntity<List<TalkSlot>>(talkSlots, HttpStatus.OK));

        mockMvc.perform(get("/talkslots/monday").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }
}
