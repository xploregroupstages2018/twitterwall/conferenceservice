package be.xplore.twitterwall.conferenceservice.service;

import be.xplore.twitterwall.conferenceservice.domain.*;
import be.xplore.twitterwall.conferenceservice.persistence.api.ConferenceRepository;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceSenderService;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceService;
import be.xplore.twitterwall.conferenceservice.service.impl.ConferenceSenderServiceImpl;
import be.xplore.twitterwall.conferenceservice.service.impl.ConferenceServiceImpl;
import be.xplore.twitterwall.conferenceservice.util.BlockingQueueStompFrameHandler;
import com.google.gson.Gson;
import com.lordofthejars.nosqlunit.mongodb.InMemoryMongoDb;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import org.junit.*;
import org.junit.runner.RunWith;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

import static com.lordofthejars.nosqlunit.mongodb.InMemoryMongoDb.InMemoryMongoRuleBuilder.newInMemoryMongoDbRule;
import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConferenceServiceUnitTests {
    @ClassRule
    public static InMemoryMongoDb inMemoryMongoDb = newInMemoryMongoDbRule().build();

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("demo-test");

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    private ConferenceService conferenceService;

    private ConferenceSenderService conferenceSenderService;

    @LocalServerPort
    private int port;

    private BlockingQueue<String> blockingQueue;

    private WebSocketStompClient webSocketStompClient;
    @Before
    public void setUp() {
        conferenceService = new ConferenceServiceImpl(conferenceRepository);
        conferenceSenderService = new ConferenceSenderServiceImpl(messagingTemplate);
        Talk talk = new Talk("For each talk in talks", new ArrayList<Tag>(), new ArrayList<Speaker>());
        Talk talk2 = new Talk("Google Search Tutorial", new ArrayList<>(), new ArrayList<>());
        talk.getSpeakers().add(new Speaker("Yolan Vloeberghs"));
        talk.getTags().add(new Tag("foreach"));
        talk2.getSpeakers().add(new Speaker("Yolan Vloeberghs"));
        talk2.getTags().add(new Tag("search"));
        TalkSlot talkSlot = new TalkSlot("08:30", "10:30", "Room 5", "tuesday", talk);
        TalkSlot talkSlot2 = new TalkSlot("11:30", "12:30", "Room 6", "tuesday", talk2);
        Conference conference = new Conference(new ArrayList<TalkSlot>(), "tuesday");
        conference.getSlots().add(talkSlot);
        conference.getSlots().add(talkSlot2);
        conferenceService.addConference(conference);

        blockingQueue = new LinkedBlockingDeque<>();
        webSocketStompClient = new WebSocketStompClient(new SockJsClient(Collections.singletonList(new WebSocketTransport(new StandardWebSocketClient()))));
    }

    @After
    public void tearDown() {
        conferenceRepository.deleteAll();
    }

    @Test
    public void testGetUpcomingSlots() {
        List<TalkSlot> slots = conferenceService.getUpcomingTalkSlots("tuesday", 60, LocalTime.of(7, 30));
        assertEquals("There should be one upcoming slot", 1, slots.size());
    }

    @Test
    public void testGetUpcomingSlotsNone() {
        List<TalkSlot> slots = conferenceService.getUpcomingTalkSlots("tuesday", 60, LocalTime.of(17, 45));
        assertFalse("There should be no upcoming talk slots", slots.size() > 0);
    }

    @Test
    public void testSendUpcomingSlotsOverWebSocket() throws InterruptedException, ExecutionException, TimeoutException {
        StompSession stompSession = webSocketStompClient.connect(
                "http://localhost:" + port + "/ws", new StompSessionHandlerAdapter() {})
                .get(1, TimeUnit.SECONDS);
        stompSession.subscribe("/topic/talks", new BlockingQueueStompFrameHandler(blockingQueue));
        List<TalkSlot> slots = conferenceService.getUpcomingTalkSlots("tuesday", 60, LocalTime.of(7, 30));
        conferenceSenderService.sendUpcomingTalkSlots(slots);

        Gson gson = new Gson();
        List<TalkSlot> sameList = gson.fromJson(blockingQueue.poll(1, TimeUnit.SECONDS), new TypeToken<List<TalkSlot>>(){}.getType());
        assertEquals("Check if the new list has the same size", slots.size(), sameList.size());
    }
}
