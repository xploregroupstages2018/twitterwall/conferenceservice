package be.xplore.twitterwall.conferenceservice.service;

import be.xplore.twitterwall.conferenceservice.domain.*;
import be.xplore.twitterwall.conferenceservice.persistence.api.ConferenceRepository;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceService;
import be.xplore.twitterwall.conferenceservice.service.exceptions.ConferenceServiceException;
import be.xplore.twitterwall.conferenceservice.service.impl.ConferenceServiceImpl;
import com.lordofthejars.nosqlunit.mongodb.InMemoryMongoDb;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.lordofthejars.nosqlunit.mongodb.InMemoryMongoDb.InMemoryMongoRuleBuilder.newInMemoryMongoDbRule;
import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConferenceServiceIntegrationTests {
    @ClassRule
    public static InMemoryMongoDb inMemoryMongoDb = newInMemoryMongoDbRule().build();

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("demo-test");

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ConferenceRepository conferenceRepository;

    private ConferenceService conferenceService;

    @Before
    public void setUp() {
        conferenceService = new ConferenceServiceImpl(conferenceRepository);
        Talk talk = new Talk("Test talk", new ArrayList<Tag>(), new ArrayList<Speaker>());
        talk.getSpeakers().add(new Speaker("Yolan Vloeberghs"));
        talk.getTags().add(new Tag("test"));
        TalkSlot talkSlot = new TalkSlot("08:30", "10:30", "Room 5", "monday", talk);
        Conference conference = new Conference(new ArrayList<TalkSlot>(), "monday");
        conference.getSlots().add(talkSlot);
        conferenceService.addConference(conference);
    }

    @After
    public void tearDown() {
        conferenceRepository.deleteAll();
    }

    @Test
    public void testGetTalksFromMonday() {
        List<TalkSlot> talkSlots = conferenceService.getAllTalkSlotsByDay("monday");
        assertEquals(1, talkSlots.size());
    }

    @Test(expected = ConferenceServiceException.class)
    public void testGetTalksFromNonValidDay() {
        conferenceService.getAllTalkSlotsByDay("sunday");
    }

    @Test
    public void testImportTalksToDatabase() {
        String base_uri = "https://cfp.devoxx.be/api/conferences/DVBE17/schedules/";
        List<String> days = Arrays.asList("monday", "tuesday", "wednesday", "thursday", "friday");
        conferenceService.importAllTalksToDatabase(base_uri, days);
        assertTrue("The database contains items from all the imported days", conferenceService.getAllTalkSlotsByDay("tuesday").size() > 1);
    }
}