package be.xplore.twitterwall.conferenceservice.util;

import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.concurrent.BlockingQueue;

/**
 * FrameHandler for STOMP that uses a BlockingQueue.
 */
public class BlockingQueueStompFrameHandler implements StompFrameHandler {

    private BlockingQueue blockingQueue;

    public BlockingQueueStompFrameHandler(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public Type getPayloadType(StompHeaders stompHeaders) {
        return byte[].class;
    }

    /**
     * Puts the incoming frame on the blockingQueue.
     * @param stompHeaders headers from stomp
     * @param o incoming object that should be offered on the queue.
     */
    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {
        blockingQueue.offer(new String((byte[]) o));
    }
}
