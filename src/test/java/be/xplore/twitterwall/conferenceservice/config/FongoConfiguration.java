package be.xplore.twitterwall.conferenceservice.config;

import com.github.fakemongo.Fongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@ComponentScan(basePackages = "be.xplore.twitterwall.conferenceservice")
@EnableMongoRepositories(basePackages = "be.xplore.twitterwall.conferenceservice.persistence.api")
@Configuration
public class FongoConfiguration extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return "demo-test";
    }

    @Bean
    @Override
    public MongoClient mongoClient() {
        return new Fongo("mongo-test").getMongo();
    }
}
