package be.xplore.twitterwall.conferenceservice.service.impl;

import be.xplore.twitterwall.conferenceservice.domain.Conference;
import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;
import be.xplore.twitterwall.conferenceservice.dto.ConferenceDto;
import be.xplore.twitterwall.conferenceservice.persistence.api.ConferenceRepository;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceService;
import be.xplore.twitterwall.conferenceservice.service.exceptions.ConferenceServiceException;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    @Autowired
    private final ConferenceRepository conferenceRepository;

    private static final Logger logger = Logger.getLogger(ConferenceService.class);

    public ConferenceServiceImpl(ConferenceRepository conferenceRepository) {
        this.conferenceRepository = conferenceRepository;
    }

    /*
     ** Imports all the data from the Devoxx API to our MongoDB service.
     ** Executes a GET call, then parses the results in DTO objects, then maps it to domain objects,
     ** then imports it in the database.
     */
    public void importAllTalksToDatabase(String base_uri, List<String> days)  {
        conferenceRepository.deleteAll();
        ModelMapper modelMapper = new ModelMapper();
        for (String day : days) {
            RestTemplate restTemplate = new RestTemplate();
            ConferenceDto conferenceDto = restTemplate.getForObject(base_uri + day, ConferenceDto.class);
            Conference conference = modelMapper.map(conferenceDto, Conference.class);
            conference.setDay(day);
            addConference(conference);
        }
        logger.info("Imported the latest API data to our database.");
    }

    @Override
    public List<TalkSlot> getAllTalkSlotsByDay(String day) {
        switch (day) {
            case "monday":
            case "tuesday":
            case "wednesday":
            case "thursday":
            case "friday":
                Conference conference = conferenceRepository.findConferenceByDay(day);
                return conference.getSlots();
            default:
                throw new ConferenceServiceException("Day is not a valid conference day.");
        }

    }

    @Override
    /*
    * Gets upcoming talk slots based on the day, max difference in minutes and the time from where we should check
    * for example: I want to get the talk slots from monday at the current time that are 60 minutes away
     */
    public List<TalkSlot> getUpcomingTalkSlots(String day, long maxDifference, LocalTime time) {
        List<TalkSlot> slots = getAllTalkSlotsByDay(day);
        List<TalkSlot> upcomingSlots = new ArrayList<>();
        for (TalkSlot slot : slots) {
            LocalTime fromTime = LocalTime.parse(slot.getFromTime());
            // Store the difference in minutes between time variable and the start time of the talk
            long difference = ChronoUnit.MINUTES.between(time, fromTime);
            // if difference is between 0 and 60 minutes
            if (difference >= 0 && difference <= maxDifference && slot.getTalk() != null) {
                upcomingSlots.add(slot);
            }
        }
        return upcomingSlots;
    }

    @Override
    public Conference addConference(Conference conference) {
        if (conference == null) {
            throw new ConferenceServiceException("Conference is null");
        }
        return conferenceRepository.save(conference);
    }
}
