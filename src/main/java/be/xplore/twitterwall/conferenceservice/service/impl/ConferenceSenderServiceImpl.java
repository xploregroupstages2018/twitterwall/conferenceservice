package be.xplore.twitterwall.conferenceservice.service.impl;

import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceSenderService;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConferenceSenderServiceImpl implements ConferenceSenderService {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    private static final Logger logger = Logger.getLogger(ConferenceSenderService.class);

    public ConferenceSenderServiceImpl(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void sendUpcomingTalkSlots(List<TalkSlot> talkSlots) {
        if (talkSlots.size() > 0) {
            // parse the array to JSON and send it through the websocket
            Gson gson = new Gson();
            String json = gson.toJson(talkSlots);
            messagingTemplate.convertAndSend("/topic/talks", json);
            logger.info("Sent new talks to frontend");
        } else {
            logger.info("No new talks are available. Retrying again later.");
        }
    }
}
