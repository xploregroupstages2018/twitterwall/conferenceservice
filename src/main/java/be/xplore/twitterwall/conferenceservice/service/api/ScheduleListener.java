package be.xplore.twitterwall.conferenceservice.service.api;

public interface ScheduleListener {
    void scheduleUpcomingTalks();
}
