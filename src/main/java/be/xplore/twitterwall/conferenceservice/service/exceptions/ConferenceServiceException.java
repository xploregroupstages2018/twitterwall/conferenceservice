package be.xplore.twitterwall.conferenceservice.service.exceptions;

public class ConferenceServiceException extends RuntimeException {
    public ConferenceServiceException(String message) {
        super(message);
    }
}
