package be.xplore.twitterwall.conferenceservice.service.api;

import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;

import java.util.List;

public interface ConferenceSenderService {
    void sendUpcomingTalkSlots(List<TalkSlot> talkSlots);
}
