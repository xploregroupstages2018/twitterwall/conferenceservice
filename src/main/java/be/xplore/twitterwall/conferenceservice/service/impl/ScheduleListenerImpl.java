package be.xplore.twitterwall.conferenceservice.service.impl;

import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceSenderService;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceService;
import be.xplore.twitterwall.conferenceservice.service.api.ScheduleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Component
public class ScheduleListenerImpl implements ScheduleListener {

    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private ConferenceSenderService conferenceSenderService;

    @Override
    @Scheduled(fixedRate = 120000)
    /*
     * Listens for changes in the schedule every X minutes.
     */
    public void scheduleUpcomingTalks() {
        String day = String.valueOf(LocalDate.now().getDayOfWeek()).toLowerCase();
        List<TalkSlot> upcomingSlots = conferenceService.getUpcomingTalkSlots(day, 60, LocalTime.now());
        conferenceSenderService.sendUpcomingTalkSlots(upcomingSlots);

    }
}
