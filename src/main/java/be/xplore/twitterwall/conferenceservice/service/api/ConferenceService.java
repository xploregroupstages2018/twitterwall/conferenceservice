package be.xplore.twitterwall.conferenceservice.service.api;

import be.xplore.twitterwall.conferenceservice.domain.Conference;
import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;

import java.time.LocalTime;
import java.util.List;

public interface ConferenceService {
    void importAllTalksToDatabase(String base_uri, List<String> days);

    Conference addConference(Conference conference);

    List<TalkSlot> getAllTalkSlotsByDay(String day);

    List<TalkSlot> getUpcomingTalkSlots(String day, long maxDifference, LocalTime time);
}
