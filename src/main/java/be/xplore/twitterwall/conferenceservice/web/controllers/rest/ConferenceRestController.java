package be.xplore.twitterwall.conferenceservice.web.controllers.rest;

import be.xplore.twitterwall.conferenceservice.domain.TalkSlot;
import be.xplore.twitterwall.conferenceservice.service.api.ConferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class ConferenceRestController {

    private final ConferenceService conferenceService;

    @Autowired
    public ConferenceRestController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @GetMapping("/migratedata")
    public ResponseEntity<String> migrateData() {
        String base_uri = "https://cfp.devoxx.be/api/conferences/DVBE17/schedules/";
        List<String> days = Arrays.asList("monday", "tuesday", "wednesday", "thursday", "friday");
        conferenceService.importAllTalksToDatabase(base_uri, days);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/talkslots/{day}")
    public ResponseEntity<List<TalkSlot>> getTalkSlotsByDay(@PathVariable("day") String day) {
        List<TalkSlot> talkSlots = conferenceService.getAllTalkSlotsByDay(day);
        return new ResponseEntity<>(talkSlots, HttpStatus.OK);
    }
}
