package be.xplore.twitterwall.conferenceservice.persistence.api;

import be.xplore.twitterwall.conferenceservice.domain.Conference;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRepository extends MongoRepository<Conference, String> {
    Conference findConferenceByDay(String day);
}
