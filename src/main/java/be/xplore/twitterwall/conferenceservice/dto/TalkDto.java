package be.xplore.twitterwall.conferenceservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TalkDto {

    private String id;

    private String title;

    private List<TagDto> tags;

    private List<SpeakerDto> speakers;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<TagDto> getTags() {
        return tags;
    }

    public List<SpeakerDto> getSpeakers() {
        return speakers;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTags(List<TagDto> tags) {
        this.tags = tags;
    }

    public void setSpeakers(List<SpeakerDto> speakers) {
        this.speakers = speakers;
    }

    @Override
    public String toString() {
        return "TalkDto{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", tags=" + tags +
                ", speakers=" + speakers +
                '}';
    }
}

