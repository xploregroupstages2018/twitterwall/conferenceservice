package be.xplore.twitterwall.conferenceservice.dto;

import java.util.List;

public class ConferenceDto {

    private String id;

    private List<TalkSlotDto> slots;

    private String day;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TalkSlotDto> getSlots() {
        return slots;
    }

    public void setSlots(List<TalkSlotDto> slots) {
        this.slots = slots;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "ConferenceDto{" +
                "id='" + id + '\'' +
                ", slots=" + slots +
                ", day='" + day + '\'' +
                '}';
    }
}
