package be.xplore.twitterwall.conferenceservice.dto;

public class TalkSlotDto {

    private String id;
    private String fromTime;
    private String toTime;
    private String roomName;
    private String day;
    private TalkDto talk;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public TalkDto getTalk() {
        return talk;
    }

    public void setTalk(TalkDto talk) {
        this.talk = talk;
    }

    @Override
    public String toString() {
        return "TalkSlotDto{" +
                "fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", roomName='" + roomName + '\'' +
                ", day='" + day + '\'' +
                ", conference=" + talk +
                '}';
    }
}
