package be.xplore.twitterwall.conferenceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tag {

    @JsonProperty
    private String value;

    public Tag(String value) {
        this.value = value;
    }

    public Tag() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
