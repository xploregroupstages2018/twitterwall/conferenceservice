package be.xplore.twitterwall.conferenceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.List;

public class Conference {
    @Id
    private String id;

    @JsonProperty
    private List<TalkSlot> slots;

    @JsonProperty
    private String day;


    public Conference(List<TalkSlot> slots, String day) {
        this.slots = slots;
        this.day = day;
    }

    public Conference() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TalkSlot> getSlots() {
        return slots;
    }

    public void setSlots(List<TalkSlot> slots) {
        this.slots = slots;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Conference{" +
                "id='" + id + '\'' +
                ", slots=" + slots +
                ", day='" + day + '\'' +
                '}';
    }
}
