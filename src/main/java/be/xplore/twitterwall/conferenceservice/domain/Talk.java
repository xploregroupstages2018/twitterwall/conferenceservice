package be.xplore.twitterwall.conferenceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class Talk {

    @JsonProperty
    private String title;

    @JsonProperty
    private List<Tag> tags;

    @JsonProperty
    private List<Speaker> speakers;

    public Talk(String title, List<Tag> tags, List<Speaker> speakers) {
        this.title = title;
        this.tags = tags;
        this.speakers = speakers;
    }

    public Talk() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Speaker> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(List<Speaker> speakers) {
        this.speakers = speakers;
    }

    @Override
    public String toString() {
        return "Talk{" +
                ", title='" + title + '\'' +
                ", tags=" + tags +
                ", speakers=" + speakers +
                '}';
    }
}
