package be.xplore.twitterwall.conferenceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "slots")
public class TalkSlot {

    @JsonProperty
    private String fromTime;

    @JsonProperty
    private String toTime;

    @JsonProperty
    private String roomName;

    @JsonProperty
    private String day;

    @JsonProperty
    private Talk talk;

    public TalkSlot(String fromTime, String toTime, String roomName, String day, Talk talk) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.roomName = roomName;
        this.day = day;
        this.talk = talk;
    }

    public TalkSlot() {
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Talk getTalk() {
        return talk;
    }

    public void setTalk(Talk talk) {
        this.talk = talk;
    }

    @Override
    public String toString() {
        return "TalkSlot{" +
                ", fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", roomName='" + roomName + '\'' +
                ", day='" + day + '\'' +
                ", conference=" + talk +
                '}';
    }
}
